PREDICTING STOCK RETURNS WITH CLUSTER-THEN-PREDICT
When selecting which stocks to invest in, investors seek to obtain good future returns. 
In this problem, we will first use clustering to identify clusters of stocks that have 
similar returns over time. Then, we'll use logistic regression to predict whether or not
the stocks will have positive future returns.

For this problem, we'll use StocksCluster.csv, which contains monthly stock returns from 
the NASDAQ stock exchange.

Each observation in the dataset is the monthly returns of a particular company in a 
particular year. The years included are 2000-2009.

Our goal will be to predict whether or not the stock return in December will be positive,
using the stock returns for the first 11 months of the year

stocks <- read.csv("StocksCluster.csv")

#INITIAL LOGISTIC REGRESSION MODEL
library(caTools)
set.seed(144)
spl = sample.split(stocks$PositiveDec, SplitRatio = 0.7)
stocksTrain = subset(stocks, spl == TRUE)
stocksTest = subset(stocks, spl == FALSE)
StocksModel <- glm(PositiveDec ~ ., data=stocksTrain, family=binomial)
trainPredict <- predict(StocksModel, type="response")

#What is the overall accuracy on the training set, using a threshold of 0.5?
table(stocksTrain$PositiveDec, trainPredict>0.5)

#Now obtain test set predictions from StocksModel. What is the overall accuracy of the 
#model on the test, again using a threshold of 0.5?
table(stocksTest$PositiveDec, testPredict>0.5)

#CLUSTERING STOCKS  
#The first step in this process is to remove the dependent variable
limitedTrain <- stocksTrain
limitedTrain$PositiveDec <- NULL
limitedTest <- stocksTest
limitedTest$PositiveDec <- NULL

#In cases where we have a training and testing set, we'll want to normalize by the mean 
#and standard deviation of the variables in the training set. 
 library(caret)
preproc <- preProcess(limitedTrain)
normTrain <- predict(preproc, limitedTrain)
normTest <- predict(preproc, limitedTest)  

#Run k-means clustering with 3 clusters on normTrain, storing the result in an object called km
set.seed(144)
km <- kmeans(normTrain,  centers=3)
str(km)

#Use the flexclust package to obtain training set and testing set cluster assignments for our observations
library(flexclust)
km.kcca = as.kcca(km, normTrain)
clusterTrain = predict(km.kcca)
clusterTest = predict(km.kcca, newdata=normTest)

#How many test-set observations were assigned to Cluster 2?
table(clusterTest)

#Using the subset function, build data frames stocksTrain1, stocksTrain2, and stocksTrain3, 
#containing the elements in the stocksTrain data frame assigned to clusters 1, 2, and 3, respectively
stocksTrain1 <- subset(stocksTrain, clusterTrain==1)
stocksTrain2 <- subset(stocksTrain, clusterTrain==2)
stocksTrain3 <- subset(stocksTrain, clusterTrain==3)

#Similarly build stocksTest1, stocksTest2, and stocksTest3 from the stocksTest data frame
stocksTest1 <- subset(stocksTest, clusterTest==1)
stocksTest2 <- subset(stocksTest, clusterTest==2)
stocksTest3 <- subset(stocksTest, clusterTest==3)

#Which training set data frame has the highest average value of the dependent variable?
mean(stocksTrain1$PositiveDec)
mean(stocksTrain2$PositiveDec)
mean(stocksTrain3$PositiveDec)

#Build logistic regression models StocksModel1, StocksModel2, and StocksModel3, which 
#predict PositiveDec using all the other variables as independent variables.
StocksModel1 <- glm(PositiveDec ~ ., data=stocksTrain1, family=binomial)
StocksModel2 <- glm(PositiveDec ~ ., data=stocksTrain2, family=binomial)
StocksModel3 <- glm(PositiveDec ~ ., data=stocksTrain3, family=binomial)

#CLUSTER-SPECIFIC PREDICTIONS
PredictTest1 <- predict(StocksModel1, newdata=stocksTest1, type="response")
PredictTest2 <- predict(StocksModel2, newdata=stocksTest2, type="response")
PredictTest3 <- predict(StocksModel3, newdata=stocksTest3, type="response")

#What is the overall accuracy of StocksModel1 on the test set stocksTest1, using a threshold of 0.5?
table(stocksTest1$PositiveDec, PredictTest1>0.5)

#What is the overall accuracy of StocksModel2 on the test set stocksTest2, using a threshold of 0.5?
table(stocksTest2$PositiveDec, PredictTest2>0.5)

#What is the overall accuracy of StocksModel3 on the test set stocksTest3, using a threshold of 0.5?
table(stocksTest3$PositiveDec, PredictTest3>0.5)

#To compute the overall test-set accuracy of the cluster-then-predict approach, we can combine 
#all the test-set predictions into a single vector and all the true outcomes into a single vector
AllPredictions <- c(PredictTest1, PredictTest2, PredictTest3)
AllOutcomes <- c(stocksTest1$PositiveDec, stocksTest2$PositiveDec, stocksTest3$PositiveDec)

#What is the overall test-set accuracy of the cluster-then-predict approach, 
#again using a threshold of 0.5?
table(AllOutcomes, AllPredictions>0.5)

We see a modest improvement over the original logistic regression model. Since predicting 
stock returns is a notoriously hard problem, this is a good increase in accuracy. By 
investing in stocks for which we are more confident that they will have positive returns
 (by selecting the ones with higher predicted probabilities), this cluster-then-predict 
 model can give us an edge over the original logistic regression model.


